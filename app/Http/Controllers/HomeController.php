<?php

namespace App\Http\Controllers;
use App\Factory\SaveForm;

use Illuminate\Http\Request;
use Validator;
class HomeController extends Controller
{
   public function send(Request $r){
    $validate = Validator::make($r->all(),[
        "name"=>"required|min:6",
        "phone"=>"required|integer|min:8",
        "message"=>"required|min:10",
    ]);

    if ($validate->fails())
    {
        return response()->json(['error' => $validate->errors()], 401);
    }
    else{
        $data = new SaveForm();
        $data->saveBd($r->name, $r->phone, $r->message);
        $data->saveFile($r->name, $r->phone, $r->message);
        return "Заявка отправлена";
    }
   }
}
